import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDatepickerInputEvent, NativeDateAdapter} from '@angular/material';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};


@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [
    {provide: DateAdapter,      useClass: DatepickerComponent},
    {provide: MAT_DATE_LOCALE,  useValue: 'es'},
    {provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS}]
})
export class DatepickerComponent extends NativeDateAdapter {

  @Input() date = new Date();
  @Output() newDate = new EventEmitter();

  serializedDate = new FormControl((new Date()).toISOString());

  format(date: Date, displayFormat: Object): string {
    switch (displayFormat.valueOf()) {
      case 'YYYY':
        return date.getFullYear().toString();
        break;
      case 'DD/MM/YYYY':

        let day = date.getDate().toString();
        if (day.length < 2) day = '0' + day;

        let month = date.getMonth().toString();
        if (month.length < 2) month = '0' + month;

        return day + '/' + month + '/' + date.getFullYear();
    }
  }

  getFirstDayOfWeek(): number {
    return 1;
  }

  setNewDate(event: MatDatepickerInputEvent<Date>) {
    this.date = event.value;
    this.newDate.emit(this.date);
  }
}



