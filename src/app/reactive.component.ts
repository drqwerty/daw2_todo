import {Component, OnInit, OnDestroy} from '@angular/core';
import {fromEvent, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, switchMap} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';
import * as _ from 'lodash';

declare var $: any; // para que sepa que ese dolar es de jquery

@Component({
  selector: 'app-reactive',
  template: `
    <input type="text" class="form-control" id="search" placeholder="search...">
    <!--<div id="images"></div>-->
    <div class="container">
      <div class="row" *ngFor="let fila of fotos">
        <div class="col-sm-4 div-img" *ngFor="let foto of fila" #divElement>
          <img [src]="foto" (click)="remove(divElement)">

        </div>
      </div>
    </div>
  `,
  styles: [`
    #search {
      margin: 20px 0;
    }

/*    #images {
      display: inline-block;
      width: 100%;
    }*/

    .div-img {
      margin-top: 2em;
      text-align: center;
    }
    .row{
      justify-content:center;
      align-items:center;
      display:flex;
    }
  `]
})
export class ReactiveComponent implements OnInit, OnDestroy {
  private flickrApi = 'https://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?';
  private su = new Subscription();

  fotos: any [];

  constructor() {
  }

  ngOnInit(): void {
    const keyups = fromEvent($('#search'), 'keyup').pipe(
      map((e: any) => e.target.value),
      filter(text => text.length > 3),
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(searchTerm => {
        const promise = $.getJSON(this.flickrApi, {
          tags: searchTerm,
          tagmode: 'all',
          format: 'json'
        });
        const observable = fromPromise(promise);
        return observable;
      })
    );

    /*this.su = keyups.subscribe(
      data => {
        $('#images').empty();
        // @ts-ignore
        $.each(data.items, (i, item) => {
          return $('<div>')
            .css({
              'height': '10em',
              'margin-bottom': '1em'
            })
            .on('click', (img) => $(img.target).parent().remove())
            .attr('class', 'col-sm-3')
            .append($('<img>')
              .attr('src', item.media.m)
              .mouseover(function () {
                $(this).css('filter', 'grayscale(100%)');
              })
              .mouseleave(function () {
                $(this).css('filter', 'grayscale(0%)');
              })
              .css({
                'height': '10em',
                'width': '100%',
                'object-fit': 'cover',
                'transition': 'filter .2s ease-in-out',
                'cursor': 'pointer'
              }))
            .appendTo('#images');
        });
      },
      (error) => console.log(error),
      () => console.log('completed!!!')
    ); // observador*/

    this.su = keyups.subscribe(
      (data: any) => {
        console.log(data);
        this.fotos = _.chunk(_.chain(data.items)
            .map(item => item.media.m)
            .value()
          , 3);
        // this.fotos = this.fotos.map(item => item.media.m);
      },
      (error) => console.log(error),
      () => console.log('completed!!!')
    ); // observador
  }

  ngOnDestroy(): void {
    this.su.unsubscribe();
  }

  remove(data: HTMLDivElement) {
    data.remove();
  }
}
