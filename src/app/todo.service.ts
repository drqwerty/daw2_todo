import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {Observable} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  public items: any = [];

  private itemsRef: AngularFireList<any>;


  constructor(private httpClient: HttpClient,
              private fdb: AngularFireDatabase,
              private fAuth: AngularFireAuth) {
    this.fAuth.authState.subscribe(
      (data) => console.log('logged in', data),
      (error) => console.log('error loggin', error),
      () => console.log('auth complete')
    );

    this.itemsRef = this.fdb.list('todo');
  }

  login() {
    this.fAuth.auth.signInWithEmailAndPassword(
      'felo@felo.felo', 'felofelofelo'
    );
  }

  logout() {
    this.fAuth.auth.signOut();
  }

  getItems() {
    return this.items.slice();
  }

  getFireItems(): Observable <any> {
    // return this.fdb.list('todo').valueChanges();
    return this.fdb
      .list('todo')
      .snapshotChanges()
      .pipe(
        map(changes => changes.map((c: any) => {
          return {
            key: c.payload.key,
            action: c.payload.val().action,
            done: c.payload.val().done,
            prioridad: c.payload.val().prioridad,
            date: c.payload.val().date
          };
        }))
      );
  }

  addItem(item) {
    this.itemsRef.push(item);
  }

  removeItem(key) {
    this.itemsRef.remove(key);
  }

  updateItem(item) {
    this.itemsRef.update(
      item.key,
      {
        action: item.action,
        done: item.done,
        prioridad: item.prioridad,
        date: item.date
      }
    );
  }
}
