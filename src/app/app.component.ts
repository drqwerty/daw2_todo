import {Component} from '@angular/core';
import {TodoService} from './todo.service';
import {MatDatepickerInputEvent} from '@angular/material';
import {DatepickerComponent} from './datepicker/datepicker.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  fecha = new Date();
  mostrarTodas = true;
  ordenarPrioridad = false;
  model = {
    user: 'Daw\'s todo list',
    items: []
  };

  constructor(private todoService: TodoService) {
    // this.model.items = todoService.getItems();
    // this.ordenaTareas();

    todoService.getFireItems().subscribe(
      resp => {
        this.model.items = resp;
        console.log('lista: ', resp);
      });
    console.log('get items', todoService.getFireItems());
    todoService.login();
  }

  TnIncompletas() {
    let count = 0;
    if (this.model.items) {
      this.model.items.forEach((item, index) => !item.done ? count++ : true);
    }

    return count;
  }

  addItem(item: string, date: DatepickerComponent) {
    if (item.length > 0) {
      // this.model.items.push({action: item, done: false, prioridad: 0});
      this.todoService.addItem({
        action: item,
        done: false,
        prioridad: 5,
        date: new Date(date.date).toISOString()
      });
    }
    // this.ordenaTareas();
  }

  findTarea(elemento) {
    return elemento.action === this; // this = action
  }

  nuevaPrioridad($event: any, action) {
    // console.log('Elemento: ' + indice + ' Prioridad: ' + $event);

    const nindice = this.model.items.findIndex(this.findTarea, action);


    this.model.items[nindice].prioridad = $event;
    this.todoService.updateItem(this.model.items[nindice]);
  }

  eliminaTarea(item) {
    const indice = this.model.items.findIndex(this.findTarea, item.action);
    // @ts-ignore
    this.todoService.removeItem(this.model.items[indice].key);
  }

  delete(index: any) {
    this.model.items.splice(index, 1);
  }

  tDone(item) {
    item.done = !item.done;
    this.todoService.updateItem(item);
  }

  // ordenaTareas() {
  //   this.model.items.sort((a: any, b: any) => {
  //     if (a.action.toLowerCase() < b.action.toLowerCase()) {
  //       return -1;
  //     } else if (a.action.toLowerCase() > b.action.toLowerCase()) {
  //       return 1;
  //     } else {
  //       return 0;
  //     }
  //   });
  // }

  updateDate($event: any, action) {
    const nindice = this.model.items.findIndex(this.findTarea, action);

    this.model.items[nindice].date = $event;
    this.todoService.updateItem(this.model.items[nindice]);
  }
}
